# Notes!
This is a readme used as a notepad, for commands or useful stuffs about django 
### Disclaimer!
This project was made in python 3.7

## Commands
To initialize the project, make sure you have installed pip (pip3 if you have python 2.7 and 3+ versions installed)

If you don't have pip installed use:
```bash
(only if you have both python and python 3 installed)
$ sudo apt-get install python3-pip
```

```bash
(only if you have python or python 3 installed)
$ sudo apt-get install python-pip
```

Then, you must install venv, this is for virtual environment, this encapsulates all the python project with his dependencies incluiding the python version used to create project

To install venv use

```bash
$ sudo apt-get install python3-venv
```

Before, to initialize the virtual environment and initialize the django framework use the following commands

```bash
$ python3 -m venv ./venv
$ pip install django
```

Later, to initialize the environment
```bash
$ source ./venv/bin/activate
```
and, to generate the django project (yes, with the dot)

```bash
$ django-admin startproject nombredelproyecto .
```

And Finally!, to start the dev server you can run the following command

```bash
$ python manage.py runserver
```

## Dockerization

### Installation
To run any docker container you must have installed Docker in you machine

[GetDocker](https://docs.docker.com/get-docker/)

And you have install too docker-compose 

[Get Docker Compose](https://docs.docker.com/compose/install/)

### Initialization

To run this project under docker container, use the following command

```bash
$ docker-compose up -d --build
```